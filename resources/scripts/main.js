console.log('Welkom, bedankt voor het bezoeken van mijn site. Binnenkort staan hier projecten waar ik aan gewerkt heb. Nu al onder de indruk? Bel me! Ik ben per 1 maart beschikbaar.');

var download = document.getElementById('download');
download.onclick = ( function()  {
		ga('send', 'event', 'Download', 'click', 'CV Download');
});

var skrollr = skrollr.init({});

loop = (function() {

	var loop = document.querySelector('[data-loop]'),
			loopItems = loop.querySelectorAll('p'),
			count = 0,
			amount = loopItems.length,
			focus = loopItems[0];

	function loopthrough() {
		focus.classList.remove('focus');
		count++;
		if (count === amount ) {
			count = 0;
		}
		focus = loopItems[count];
		focus.classList.add('focus');
	}

setInterval(loopthrough, 5000);

})();

//do it with jQuery
//(function($) {

	// $('[data-loopthrough]').slick({
	// 	autoplay: true,
	// 	autoplaySpeed: 5000,
	// 	arrows: false,
	// 	fade: true,
	// 	speed: 500
	// });

	// $('[data-case-images]').slick({
	// 	autoplay: true,
	// 	autoplaySpeed: 6000
	// });

	// $('[data-case-info]').slick({
	// 	autoplay: true,
	// 	autoplaySpeed: 6000
	// });


//})(jQuery);
